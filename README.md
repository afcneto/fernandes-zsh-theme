# Tema "fernandes" para o oh-my-zsh

- Personalização de tema para ser usado no oh-my-zsh.

```
Para usá-lo, apenas realiza a copia do arquivo "fernandes.zsh-theme" na seguinte pasta:

~$ .oh-my-zsh/themes/

```
