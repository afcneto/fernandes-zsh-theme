## ZSH Theme - https://github.com/afcneto/fernandes-zsh-theme

return_code="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"

PROMPT="%{$fg[white]%}%~%  \$(git_prompt_info)%{$reset_color%}%B»%b "
RPS1="${return_code}"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[yellow]%}‹"
ZSH_THEME_GIT_PROMPT_SUFFIX="› %{$reset_color%}"

unset return_code host_color